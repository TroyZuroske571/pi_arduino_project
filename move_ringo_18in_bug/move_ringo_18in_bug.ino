/*

Ringo Robot
Ringo_Base_Sketch_01
Version 2.0 8/2015

This is a basic sketch that can be used as a starting point
for various functionality of the Ringo robot.

This code was written by Plum Geek LLC with most
significant portions written by Dustin Soodak.
Portions from other open source projects where noted.
This code is licensed under:
Creative Commons Attribution-ShareAlike 2.0 Generic (CC BY-SA 2.0)
https://creativecommons.org/licenses/by-sa/2.0/
Visit http://www.plumgeek.com for Ringo information.
Visit http://www.arduino.cc to learn about the Arduino.

*/

#include "RingoHardware.h"
#include "FunStuff.h"
#include "IRCommunication.h"

#define MSG_SIZE 4  // Set to 4 since I'm using IR to communicate with the remote also

byte myID = 0x0A;

byte msg[MSG_SIZE] = {myID, 0x12, 0x34,0x56};
//msg[0] = myID; // send myID around the ring for election
//msg[1] = 0x12;

void calibrate_edge_seosors();

void setup(){
  HardwareBegin();        //initialize Ringo's brain to work with his circuitry
  Serial.begin(9600);
  PlayStartChirp();       //Play startup chirp and blink eyes
  //SwitchMotorsToSerial(); //Call "SwitchMotorsToSerial()" before using Serial.print functions as motors & serial share a line
  //RestartTimer();

  digitalWrite(Source_Select, LOW);  //select top side light sensors
  calibrate_edge_seosors();

  RxIRRestart(4);         //wait for 4 byte IR remote command
  IsIRDone();
  GetIRButton();

  ResetIR(MSG_SIZE);
}

int leftOn, leftOff, rightOn, rightOff, rearOn, rearOff; //declaire variables
int leftDiff, rightDiff, rearDiff; //more variables

float leftWhiteAvg, rightWhiteAvg, rearWhiteAvg;
int leftMin, rightMin, rearMin;

bool frontOnWhite = true;
bool backOnWhite = true;
bool rearOnWhite = true;

bool calibrated = false;

int MARGIN = 100;

void read_edge_sensors() {

  digitalWrite(Edge_Lights, HIGH); // turn on the IR light sources
  delayMicroseconds(200); // let sensors stabalize

  leftOn = analogRead(LightSense_Left); // read sensors w/ IR lights on
  rightOn = analogRead(LightSense_Right); // read sensors w/ IR lights on
  rearOn = analogRead(LightSense_Rear); // read sensors w/ IR lights on

  digitalWrite(Edge_Lights, LOW); // turn off the IR light sources
  delayMicroseconds(200); // let sensors stabalize

  leftOff = analogRead(LightSense_Left); // read sensors w/ IR lights off
  rightOff = analogRead(LightSense_Right); // read sensors w/ IR lights off
  rearOff = analogRead(LightSense_Rear); // read sensors w/ IR lights off

  leftDiff = leftOn-leftOff; // subtract out ambient light
  rightDiff = rightOn-rightOff; // subtract out ambient light
  rearDiff = rearOn-rearOff; // subtract out ambient light
}

void calibrate_edge_seosors() {
  int leftTot = 0;
  int rightTot = 0;
  int rearTot = 0;

  leftMin = 1024;
  rightMin = 1024;
  rearMin = 1024;
  
  int i = 0;
  int SAMPLES = 20;

  for ( i = 0; i < SAMPLES; i++) {
    read_edge_sensors();
    leftTot += leftDiff;
    rightTot += rightDiff;
    rearTot += rearDiff;

    leftMin = min(leftMin, leftDiff);
    rightMin = min(rightMin, rightDiff);
    rearMin = min(rearMin, rearDiff);
    //delay(250);
  }

  leftWhiteAvg = leftTot / float(SAMPLES);
  rightWhiteAvg = rightTot / float(SAMPLES);
  rearWhiteAvg = rearTot / float(SAMPLES);

  Serial.println(""); 
  Serial.print(leftWhiteAvg); Serial.print("  "); Serial.print(leftMin); Serial.println(""); 
  Serial.print(rightWhiteAvg); Serial.print("  "); Serial.print(rightMin); Serial.println(""); 
  Serial.print(rearWhiteAvg); Serial.print("  "); Serial.print(rearMin); Serial.println(""); 
}


void loop(){
  //SendIRMsg(myID, msg, MSG_SIZE);
  //delay(1000);
  //ResetIR(MSG_SIZE);

  if(IsIRDone()){                   //wait for an IR remote control command to be received
    
    if ( ! calibrated ) {
      calibrate_edge_seosors();
      calibrated = true;
    }
    
    read_edge_sensors();

    byte button = GetIRButton();       // read which button was pressed, store in "button" variable

    switch (button) {
      case 1:
          while ( leftDiff > leftMin - MARGIN || rightDiff > rightMin - MARGIN) { // Front is on white
            Motors(30, 30);
            read_edge_sensors();
          }
          // Front saw a line
          
          while ( leftDiff < leftMin - MARGIN || rightDiff < rightMin - MARGIN ) { // Front is on black (line)
            Motors(30, 30);
            read_edge_sensors();
          }

          // Front has passed the line
          
          //delay(500);
          Motors(0, 0);
          break;
    }

    SendIRMsg(myID, msg, MSG_SIZE);
    
    //SwitchMotorsToSerial();
    //Serial.print("MESSAGE:  " );Serial.print(msg[0],HEX);Serial.print(" ");Serial.print(msg[1],HEX);Serial.print(" ");Serial.println(msg[2],HEX);Serial.println();
    //SwitchSerialToMotors();
    
    ResetIR(MSG_SIZE);

  /*
    Serial.print(leftOn); // print the results to the serial window
    Serial.print("-"); // print the results to the serial window
    Serial.print(leftOff); // print the results to the serial window
    Serial.print("="); // print the results to the serial window
    Serial.print(leftDiff); // print the results to the serial window
    Serial.print(" ");
  
    Serial.print(rightOn); // print the results to the serial window
    Serial.print("-"); // print the results to the serial window
    Serial.print(rightOff); // print the results to the serial window
    Serial.print("="); // print the results to the serial window
    Serial.print(rightDiff); // print the results to the serial window
    Serial.print(" ");
  
    Serial.print(rearOn); // print the results to the serial window
    Serial.print("-"); // print the results to the serial window
    Serial.print(rearOff); // print the results to the serial window
    Serial.print("="); // print the results to the serial window
    Serial.print(rearDiff); // print the results to the serial window
    Serial.println(""); // blank line with carraige return
  *///delay(250); // wait 1/4 second before doing it again
    //delay(2000); // wait 1/4 second before doing it again
  }
}










