"""
CIS 650
SPRING 2016

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
# flag.py
"""
import sys
import time
import paho.mqtt.client as mqtt
from math import ceil

#############################################
## Get UID and upstream_UID from args
#############################################
"""
if len(sys.argv) != 2:
	print('ERROR\nusage: neighbor.py <int: ID> ')
	sys.exit()

try:
    UID = int(sys.argv[1])  # UID is the neighbor's ID (either 1 or 2)
except ValueError:
    print('ERROR\nusage: neighbor.py <int: ID> ')
    sys.exit()
"""
UID = 'flag_new'
###############################
# Class Variables & Functions #
###############################
flag = [0, "0", "0"]
turn = "1"


#############################################
## MQTT settings
#############################################

broker      = 'green0'
port        = 1883

# subscribe topics
setflag1_topic = 'setflag/1'
setflag2_topic = 'setflag/2'
setturn_topic = 'setturn'
get_vars_topic = 'getvars'

# publish topics
flag1_topic = 'flag/1'
flag2_topic = 'flag/2'
turn_topic = 'turn'
will_topic = 'will/'
vars1_topic = 'sendvars/1'
vars2_topic = 'sendvars/2'

#quality of service
qos = 2

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on token_topic
def on_flag1(client, userdata, msg):
    global flag
    flag[1] = str(msg.payload)
    client.publish(flag1_topic, str(msg.payload))
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)

def on_flag2(client, userdata, msg):
    global flag
    flag[2] = str(msg.payload)
    client.publish(flag2_topic, str(msg.payload))
    print("Received message: " + str(msg.payload) + "on topic: " + msg.topic)

def on_turn(client, userdata, msg):
    global turn
    turn = str(msg.payload)
    client.publish(turn_topic, str(msg.payload))
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')

  # variables in order of Flag1, Flag2, Turn, e.g. "101"
def send_vars(client,farmer_id):
    vars = flag[1] + flag[2] + turn
    if (farmer_id == "1"):
        client.publish(vars1_topic, vars)
    if (farmer_id == "2"):
        client.publish(vars2_topic, vars)

def on_vars_request(client, userdata, msg):
    send_vars(client, str(msg.payload))


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = "Dead UID: {}".format(UID)
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(setflag1_topic, on_flag1)
    client.message_callback_add(setflag2_topic, on_flag2)
    client.message_callback_add(setturn_topic, on_turn)
    client.message_callback_add(get_vars_topic, on_vars_request)
    client.message_callback_add(will_topic, on_will)

    # connect to broker
    client.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    client.subscribe([(setflag1_topic, qos),
                      (setflag2_topic, qos),
                      (setturn_topic, qos),
                      (get_vars_topic, qos),
                      (will_topic, qos),
                      ])
    time.sleep(5)
    client.publish(flag1_topic, flag[1])
    client.publish(flag2_topic, flag[2])
    client.publish(turn_topic, turn)

    client.loop_forever()

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()
